import pygame, pickle, math, json
import core
from frect import fRect

'''
All dimensions are specified in meters
'''
class Map:
	def __init__(self, width=0.0, height=0.0, name=""):
		self.width = float(width)
		self.height = float(height)
		self.name = name
		self.sector_count_x = None
		self.sector_count_y = None
		self.sector_width = None
		self.sector_height = None
	
	''' 
	We move GameController dependent code here. Once a Map instance has been created,
	the GameController will initialize it.
	'''
	def initialize(self):
		self._init_sectors(800, 600)
		
	def _init_sectors(self, width, height):
		self.sectors = []
		self.sector_width = width
		self.sector_height = height
		self.sector_count_x = int(math.ceil(self.width/width))
		self.sector_count_y = int(math.ceil(self.height/height))
		for x in range(self.sector_count_x):
			self.sectors.append([])
			for y in range(self.sector_count_y):
				self.sectors[x].append({'static': pygame.sprite.Group(), \
				'dynamic': pygame.sprite.Group(), \
				'bounds': fRect(x*self.sector_width, y*self.sector_height, self.sector_width, self.sector_height)})
		
	
	def insert_static_object(self, object):
		sec_bounds = self._get_sector_boundaries_for_rect(object.frect)
		
		for i in range(sec_bounds['right']-sec_bounds['left']+1):
			for j in range(sec_bounds['bottom']-sec_bounds['top']+1):
				self.sectors[sec_bounds['left']+i][sec_bounds['top']+j]['static'].add(object)
	
	
	def remove_static_object(object):
		sec_bounds = self._get_sector_boundaries_for_rect(object.rect)
		
		for i in range(sec_bounds['right']-sec_bounds['left']+1):
			for j in range(sec_bounds['bottom']-sec_bounds['top']+1):
				self.sectors[sec_bounds['left']+i][sec_bounds['top']+j]['static'].remove(object)
		
	
	def load_map(self, filename):
		file = open(filename, 'r')
		js = file.read()
		lvl = json.loads(js)
		
		self.width = lvl['size'][0]
		self.height = lvl['size'][1]
		self.name = lvl['name']
		
		def __get_class(class_name):
			parts = class_name.split('.')
			module = ".".join(parts[:-1])
			m = __import__(module)
			for comp in parts[1:]:
				m = getattr(m, comp)
			return m
	
		def __build_object(obj_dict):
			obj = __get_class(obj_dict['type'])
			obj_dict.pop('type')
			return obj(**obj_dict)
			
		if lvl['objects'].get('static'):
			for obj_dict in lvl['objects']['static']: 
				self.insert_static_object(__build_object(obj_dict)) 
			
		# TODO: dynamic objects
	
	
	def _get_sector_boundaries_for_rect(self, frect):	
		def __fix_boundaries(val, x_or_y):
			if x_or_y == 'x':
				count = self.sector_count_x
			elif x_or_y == 'y':
				count = self.sector_count_y
			
			if val < 0:
				val = 0
			elif val >= count:
				val = count-1
			
			return val

		left = __fix_boundaries(int(math.floor(frect.left*self.sector_count_x/self.width)), 'x')
		right = __fix_boundaries(int(math.floor(frect.right*self.sector_count_x/self.width)), 'x')
		top = __fix_boundaries(int(math.floor(frect.top*self.sector_count_y/self.height)), 'y')
		bottom = __fix_boundaries(int(math.floor(frect.bottom*self.sector_count_y/self.height)), 'y')
		
		return {'left': left, 'right': right, 'top': top, 'bottom': bottom}
		
		
	def get_visible_objects(self, rect):
		sec_bounds = self._get_sector_boundaries_for_rect(rect)
		
		# Create list of references to visible sectors
		visible_sectors = []
		for i in range(sec_bounds['right']-sec_bounds['left']+1):
			for j in range(sec_bounds['bottom']-sec_bounds['top']+1):
				group = self.sectors[sec_bounds['left']+i][sec_bounds['top']+j]['static']
				visible_sectors.append(group)
				
		w = (self.sectors[sec_bounds['right']][sec_bounds['top']]['bounds'].right - self.sectors[sec_bounds['left']][sec_bounds['top']]['bounds'].left)
		h = (self.sectors[sec_bounds['left']][sec_bounds['bottom']]['bounds'].bottom - self.sectors[sec_bounds['left']][sec_bounds['top']]['bounds'].top)
		area = fRect(self.sectors[sec_bounds['left']][sec_bounds['top']]['bounds'].left, self.sectors[sec_bounds['left']][sec_bounds['top']]['bounds'].top, w, h)

		return  area, visible_sectors
			

	@classmethod
	def load_map_from_pickle_file(self, filename):
		mapfile = open(filename, 'r')
		return pickle.load(mapfile)
	
	
	def export_map_to_pickle_file(self, filename=None):
		if not filename:
			filename = self.name.lower().strip().replace(' ', '_')
		f = open(filename, 'w')
		pickle.dump(self, f)