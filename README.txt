Terrengine:
----------
Terrengine is a small 2d procedural terrain engine written in python using pygame. It is designed to create playable sidescrolling terrain environments similar to those in terraria. I plan on integrating Box2D physics in the engine and using it to make an exploration based game using physics. 

To run, execute main.py, wait a few seconds for the terrain to load, and use the arrow keys to navigate.
  
ISSUE LIST:	
[ ]	4) Box2D integration
		* Map has a b2world object
		* Physics component of objects in map handled by b2world
		* Figure out where to call step()... try and avoid adding update() to Map
		
[ ]	5) Update StaticObject to have collision polygon
		* Add colision polygon component to StaticObject
		* Add any updating code necessary
	
[ ]	6) Create ActiveObject class:
		* Has a collision polygon 
		* Box2D wrappers to handle physics: