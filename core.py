import pygame
from map import Map
from camera import Camera
from frect import fRect

CAMERA_WIDTH = 800
CAMERA_HEIGHT = 600


class GameController:
	def __init__(self):
		self.camera = Camera(30, 30, 10.0, (10.0, 10.0))
		self.map = Map(80, 60)
	
	def initialize(self):
		self.map.initialize()
		self.camera.initialize()
		
def get_map():
	return game_controller.map

def get_camera():
	return game_controller.camera

def draw(background):
	game_controller.camera.draw_visible_objects(background)

game_controller = GameController()
game_controller.initialize()