import pygame
from frect import fRect

class GameObject(pygame.sprite.Sprite):
	def __init__(self, frect, image, solid=True, visible=True, collide_rect=None):
		pygame.sprite.Sprite.__init__(self)
		self.frect = frect
		self.image = image
		self.solid = solid
		self.collide_rect = collide_rect
		self.visible = visible
		if self.solid and not self.collide_rect:
			self.collide_rect = self.frect

	
class Brick(GameObject):
	def __init__(self, **kwargs):
		self.color = tuple(kwargs['color'])
		frect = fRect(kwargs['rect'][0], kwargs['rect'][1], kwargs['rect'][2], kwargs['rect'][3])
		img = pygame.Surface((frect.width, frect.height))
		img.fill(self.color)
		GameObject.__init__(self, frect, img)