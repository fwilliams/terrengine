import pygame, random, math
import perlin

pygame.init()
perlin.init()

TILE_SIZE = 2
MAP_SIZE = (500, 500)
MAP_SIZE_X = MAP_SIZE[0]
MAP_SIZE_Y = MAP_SIZE[1]
f_MAP_SIZE_X = float(MAP_SIZE_X)
f_MAP_SIZE_Y = float(MAP_SIZE_Y)

def load_map(width, height): 
	map = []
	row = []
	for i in range(0, height):
		row = []
		for item in range(0, width):
			row.append(0)
		map.append(row)
	
	return map

def set_block_range(density):
	if density < .25:
		return 0.0
	elif density > .25 and density < .5:
		return 1.0
	elif density < .75 and density > .5:
		return 2.0
	elif density > .75 and density < 1:
		return 3.0
		

def generate_noise(map):
	depth = f_MAP_SIZE_Y
	
	for width in range(0, MAP_SIZE_X):
		depth = f_MAP_SIZE_Y

		for d in range(0, MAP_SIZE_Y):
			u = depth/MAP_SIZE_Y
			#u = math.fabs(math.sin(depth/f_MAP_SIZE_Y))

			density  = random.uniform(0, math.sin(u))
			map[(MAP_SIZE_Y-1)-d][width] = (set_block_range(density)/4.0)
			#map[(MAP_SIZE_Y-1)-d][width] = density
			#print density
			depth -= 1

def fill_surface(map, bg):
	average = -1
	tops = []
	for col in range(0, MAP_SIZE_X):
		for row in range(0, MAP_SIZE_Y):
			if map[row][col] != 0:
				tops.append(row)
				break
	if(len(tops) == MAP_SIZE_X):
		average = int(sum(tops)/MAP_SIZE_X)+1
		pygame.draw.line(bg, (255, 0, 0), (0, average*TILE_SIZE), (TILE_SIZE*MAP_SIZE_X, average*TILE_SIZE))
		print 'Average %f' %average
	else: 
		print 'Bad tops len %d' %len(tops)
	
	interpolate = []
	for top in range(0, MAP_SIZE_X):
		if tops[top] < average:
			interpolate.append((top, tops[top]))

	print interpolate
	print len(interpolate)
	for t in range(0, len(interpolate)-1):
		t1 = interpolate[t][0]
		t2 = interpolate[t+1][0]
		
		v1 = interpolate[t][1]
		v2 = interpolate[t+1][1]
		
		rng = t2-t1
		for a in range(0, rng):
			# cosine interpolation
			#b = math.cos(math.pi*a)/2 + 0.5
			b = a/rng # linear interpolation
			i = (v1*(1-(b)) + v2*(b))
			print (i,t1+a) 
			rect = pygame.Rect((t1+a)*TILE_SIZE, i*TILE_SIZE, TILE_SIZE, TILE_SIZE)
			pygame.draw.rect(background, (0, 0, .25*255), rect)
			map[int(i)][int(t1+a)] = 0.25
			
			for z in range(0, 2*(average-i)):
				print z
				rect = pygame.Rect((t1+a)*TILE_SIZE, (i+z)*TILE_SIZE, TILE_SIZE, TILE_SIZE)
				pygame.draw.rect(background, (0, 0, .25*255), rect)
				map[int(i+z)][int(t1+a)] = 0.25

	return average	
	
	
		
def build_cave(map):
	pass
	

def draw_map(background, map, oct, amp):
	left = 0
	top = 0
	max = 0
	a = amp
	for z in range(0, oct):
		max += a
		print "A " + str(a)
		a /= 2
	
	print max
	
	for row in map:
		left = 0
		for point in row:
			point = (point/max)
			
			if point < .5:
				point = 0.0
			elif point > .5 and point < .6:
				point = .25
			elif point < .6 and point > .7:
				point = .5
			elif point > .7 and point < .8:
				point = .75
			elif point > .8:
				point = .9
			
			rgb = (0, 0, (point*255))
			#print rgb
			#if point == 0:
				#rgb = (100, 100, 200)
				
			rect = pygame.Rect(left*TILE_SIZE, top*TILE_SIZE, TILE_SIZE, TILE_SIZE)
			pygame.draw.rect(background, rgb, rect)
			left += 1
		top += 1

			
game_running = True

screen = pygame.display.set_mode((TILE_SIZE*MAP_SIZE_X, TILE_SIZE*MAP_SIZE_Y))
pygame.display.set_caption('Cave generator')

background = pygame.Surface(screen.get_size())
background = background.convert()
background.fill((50, 50, 50))

clock = pygame.time.Clock()

map = load_map(MAP_SIZE_X, MAP_SIZE_Y)
#generate_noise(map)
perlin.perlin_2d(16.0, 1.0, 4, map)
draw_map(background, map, 3, 1.0)

'''
while 1:
	average  = fill_surface(map, background)
	if fill_surface(map, background) == average:
		break
'''
build_cave(map)


while game_running:
	clock.tick(30)
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			game_running = False
		if event.type == pygame.KEYDOWN:
			perlin.linear_interpolate(16.0, 16.0, map)
	screen.blit(background, (0,0))
	pygame.display.flip()
	
	