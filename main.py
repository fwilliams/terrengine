import pygame, random, math
import perlin

pygame.init()
perlin.init()

TILE_SIZE = 10
MAP_SIZE = (300, 300)
MAP_SIZE_X = MAP_SIZE[0]
MAP_SIZE_Y = MAP_SIZE[1]
f_MAP_SIZE_X = float(MAP_SIZE_X)
f_MAP_SIZE_Y = float(MAP_SIZE_Y)

def create_map(width, height): 
	map = []
	row = []
	for i in range(0, height):
		row = []
		for item in range(0, width):
			row.append(0)
		map.append(row)
	
	return map


ground = []
for i in range(0, MAP_SIZE_X):
	ground.append(0)

	
def draw_map(background, map, oct, amp):
	left = 0
	top = 0
	max = 0
	for z in range(0, oct):
		max += amp
		amp /= 2
	for row in map:
		left = 0
		for point in row:
			point = (point/max)
			
			if point < .45:
				point = 0.0
			elif point > .45 and point < .6:
				point = .8
			elif point < .6 and point > .7:
				point = .6
			elif point > .7 and point < 8:
				point = .4
			elif point > .8:
				point = .2
			
			rgb = ((point*255), (point*255)/1.2, (point*255)/2)
			rect = pygame.Rect(left*TILE_SIZE, top*TILE_SIZE, TILE_SIZE, TILE_SIZE)
			pygame.draw.rect(background, rgb, rect)
			left += 1
		top += 1
	'''
	for pt in range(0, len(ground)):
		rgb = (0, 200, 0)
		rect = pygame.Rect(pt*TILE_SIZE, int(ground[pt])*TILE_SIZE, TILE_SIZE, TILE_SIZE)
		pygame.draw.rect(background, rgb, rect)
	'''
		


def build_cave(map):
	#perlin.perlin_1d(128.0, 2.0, 2, ground)
	perlin.perlin_2d(24.0, 100.0, 4, map)
	draw_map(background, map, 4, 100.0)
	
			
game_running = True

#screen = pygame.display.set_mode((TILE_SIZE*MAP_SIZE_X, TILE_SIZE*MAP_SIZE_Y))

screen = pygame.display.set_mode((800, 600))

pygame.display.set_caption('Cave generator')

#background = pygame.Surface(screen.get_size())

background = pygame.Surface((TILE_SIZE*MAP_SIZE_X, TILE_SIZE*MAP_SIZE_Y))

background = background.convert()
background.fill((50, 50, 50))

clock = pygame.time.Clock()

map = create_map(MAP_SIZE_X, MAP_SIZE_Y)
build_cave(map)
left = 0
top = 0

while game_running:
	clock.tick(30)
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			game_running = False
		#if event.type == pygame.KEYDOWN:
			
	key = pygame.key.get_pressed()	
	if key[pygame.K_RIGHT]:
		left -= 10
		if left+TILE_SIZE*MAP_SIZE_X < 800:
			left = -TILE_SIZE*MAP_SIZE_X+800
	if key [pygame.K_LEFT]:
		left+= 10
		if left > 0:
			left = 0
	if key[pygame.K_DOWN]:
		top -= 10
		if top+TILE_SIZE*MAP_SIZE_Y < 600:
			top = -TILE_SIZE*MAP_SIZE_Y+600
	if key[pygame.K_UP]:
		top += 10
		if top > 0:
			top = 0
	screen.blit(background, (left,top))
	pygame.display.flip()
	
	