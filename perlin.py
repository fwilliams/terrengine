import random
import math

def init():
	random.seed()

def linear_interpolate_2d(sample_width, sample_height, set):
	sample_size_x = len(set)/sample_width
	sample_size_y = len(set[0])/sample_height
	x_len = len(set)
	y_len = len(set[0])
	
	for x in range(0, int(sample_size_x)):
		for y in range(0, int(sample_size_y)):
			for i in range(0, int(sample_width)):
				for j in range(0, int(sample_height)):
					x1 = int((x)*sample_width)
					x2 = int((x+1)*sample_width)
					y1 = int((y)*sample_height)
					y2 = int((y+1)*sample_height)
						
					f11 = set[x1][y1]
					f21 = set[x2%x_len][y1]
					f12 = set[x1][y2%y_len]
					f22 = set[x2%x_len][y2%y_len]
					
					yr = y1+j
					xr = x1+i
					
					set[xr][yr] = f11*((x2-xr)*(y2-yr))/((x2-x1)*(y2-y1)) + f21*((xr-x1)*(y2-yr))/((x2-x1)*(y2-y1)) + f12*((x2-xr)*(yr-y1))/((x2-x1)*(y2-y1)) + f22*((xr-x1)*(yr-y1))/((x2-x1)*(y2-y1))
				
def perlin_2d(start_period, start_amplitude, octave_count, set):
	# Assume set is all zeroes
	period = float(start_period)
	amplitude = float(start_amplitude)
	
	
	for octave in range(0, octave_count):
		for x in range(0, int(len(set)/period)):
			for y in range(0, int(len(set[0])/period)):
				set[int(x*period)][int(y*period)] += random.uniform(0, amplitude)
		linear_interpolate_2d(period, period, set)
		amplitude /= 2
		period /= 2
		

def cosine_interpolate_1d(sample_width, set):
	sample_size_x = len(set)/sample_width
	for x in range(0, int(sample_size_x)):
		for i in range(0, int(sample_width)):
			x1 = int((x)*sample_width)
			x2 = int((x+1)*sample_width)
			xr = x+i
			
			ft = xr*math.pi
			f = (1-math.cos(ft))*.5
			
			set[xr] = set[x1]*(x2-xr)/(x2-x1) + set[x2%len(set)]*(xr-x1)/(x2-x1)
			
		
	
def perlin_1d(start_period, start_amplitude, octave_count, set):
	# Assume set is all zeroes
	period = float(start_period)
	amplitude = float(start_amplitude)
	
	for octave in range(0, octave_count):
		for x in range(0, int(len(set)/period)):
			set[int(x*period)] += random.uniform(0, amplitude)
		cosine_interpolate_1d(period, set)
		amplitude /= 2
		period /= 2
	
	