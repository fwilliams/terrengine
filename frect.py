'''
A basic floating point rect class mimmicing pygame's Rect class

TODO:
	* Add full functionality of pygame Rect class
	* Add printing method
'''
class fRect:
	def __init__(self, left, top, width, height):
		self.left = float(left)
		self.top = float(top)
		self.width = float(width)
		self.height = float(height)
		self._update_params()

	def _update_params(self):
		self.right = self.left + self.width
		self.bottom = self.top + self.height
		self.size = (self.width, self.height)
		
	def copy(self):
		return fRect(self.left, self.top, self.width, self.height)
	
	def move(self, x, y):
		return fRect(self.left+x, self.top+y, self.width, self.height)
		
	def move_ip(self, x, y):
		self.left += x
		self.top += y
		self._update_params()
		
	def inflate(self, x, y):
		return fRect(self.left-x/2, self.top-x/2, self.width+x, self.height+y)
	
	def inflate_ip(self, x, y):
		self.left -= x/2
		self.top -= x/2
		self.width += x
		self.height+= x
		self._update_params()
	
	def contains(self, rect):
		if self.width < rect.width:
			return False
		if self.height < rect.height:
			return False
		if self.left > rect.left:
			return False
		if self.right < rect.right:
			return False
		if self.top > rect.top:
			return False
		if self.bottom < rect.bottom:
			return False
		return True