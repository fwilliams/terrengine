import pygame, math
import core
from frect import fRect

'''
TODO: Define view angle, and distance from map
'''
class Camera:
	def __init__(self, angle_x, angle_y, distance, map_position):
		self.angle_x = angle_x
		self.angle_y = angle_y
		self.distance = distance
		self.position = map_position
		
		w = self._calculate_rect_sidelength(angle_x, distance)
		h = self._calculate_rect_sidelength(angle_y, distance)
		pos = (map_position[0] - w/2, map_position[1] - w/2)
		
		self.view_rect = fRect(pos[0], pos[1], w, h)
		self.buffer_rect = self.view_rect.inflate(self.view_rect.width/2, self.view_rect.height/2)
			
		self._visible_objects = []
		self._view_area = None
	
	'''
	If components from core module required in intialization, put code here.
	'''
	def initialize(self):
		self._update()
	
	def _calculate_rect_sidelength(self, ang, dist):
		print dist*math.tan(math.radians(ang))*2
		return dist*math.tan(math.radians(ang))*2

	'''
	Not thread safe; Need a mutex around _visible_objects if multithread
	'''
	def _update(self):
		map = core.get_map()
		self._view_area, self._visible_objects = map.get_visible_objects(self.buffer_rect)
	
	def draw_visible_objects(self, screen):
		scalesurf = pygame.Surface((800, 600))
		surf = pygame.Surface((self.view_rect.width, self.view_rect.height))
		
		for grp in self._visible_objects:
			for spr in grp.sprites():
				rel_pos = (spr.frect.left - self.view_rect.left, spr.frect.top - self.view_rect.top)
				surf.blit(spr.image, ((spr.frect.left-self.view_rect.left), (spr.frect.top-self.view_rect.top)))
		
		scale_surf = pygame.transform.scale(surf, (800, 600))
		screen.blit(scale_surf, (0, 0))
		
	def move(self, dx, dy):
		self.view_rect.move_ip(dx, dy)
		self.buffer_rect.move_ip(dx, dy)
		self.position = (self.position[0] + dx, self.position[1] + dy)
		if not self._view_area.contains(self.buffer_rect):
			self._update()
	
	def zoom(self, dz):
		self.distance += dz
		w = self._calculate_rect_sidelength(self.angle_x, self.distance)
		h = self._calculate_rect_sidelength(self.angle_y, self.distance)
		
		self.view_rect.width = w
		self.view_rect.height = h
		self.view_rect.left = self.position[0]
		self.view_rect.top = self.position[1]
		
	def get_visible_objects(self):
		return self._visible_objects
	
	'''
	This method is for debug purposes only
	'''
	def _draw_example_rects(self, screen):
		s1 = pygame.Surface(self._view_area.size)
		pygame.draw.rect(s1, (255, 0, 0), pygame.Rect((0, 0), self._view_area.size), 2)
		
		s2 = pygame.Surface(self.buffer_rect.size)
		pygame.draw.rect(s2, (0, 255, 0), pygame.Rect((0, 0), self.buffer_rect.size), 2)
		
		s3 = pygame.Surface(self.frect.size)
		pygame.draw.rect(s3, (0, 0, 255), pygame.Rect((0, 0), self.frect.size), 2)
		
		screen.blit(s1, (self._view_area.left, self._view_area.top))
		screen.blit(s2, (self.buffer_rect.left, self.buffer_rect.top))
		screen.blit(s3, (self.frect.left, self.frect.top))