import core
import pygame 

pygame.init()

game_running = True

screen = pygame.display.set_mode((800, 600))

pygame.display.set_caption('engine test')

background = pygame.Surface(screen.get_size())
background = background.convert()
background.fill((0, 100, 0))

clock = pygame.time.Clock()

core.get_map().load_map('testmap.lvl')

while game_running:
	clock.tick(30)
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			game_running = False
	
	key = pygame.key.get_pressed()	
	if key[pygame.K_RIGHT]:
		core.get_camera().move(1.0, 0)
	if key [pygame.K_LEFT]:
		core.get_camera().move(-1.0, 0)
	if key[pygame.K_DOWN]:
		core.get_camera().move(0, 1.0)
	if key[pygame.K_UP]:
		core.get_camera().move(0, -1.0)
	if key[pygame.K_z]:
		core.get_camera().zoom(1.0)
	if key[pygame.K_x]:
		core.get_camera().zoom(-1.0)
			
	
	screen.blit(background, (0,0))
	core.draw(screen)
	pygame.display.flip()